package es.um.calculator.service;

import es.um.calculator.client.GraderClient;
import es.um.calculator.client.GreenWebClient;
import es.um.calculator.client.WebsiteCarbonClient;
import es.um.calculator.domain.dto.GraderResponse;
import es.um.calculator.domain.dto.GreenWebHostingResponse;
import es.um.calculator.domain.dto.WebsiteCarbonResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
class CalculatorServiceTests {

    @Autowired
    private CalculatorService calculatorService;

    @MockBean
    private GreenWebClient greenWebClient;
    @MockBean
    private WebsiteCarbonClient websiteCarbonClient;
    @MockBean
    private GraderClient graderClient;

    @Test
    void givenUrl_whenCalculate_thenReturnStats() throws MalformedURLException {
        when(greenWebClient.getHosting(any())).thenReturn(
                ResponseEntity.ok(
                        GreenWebHostingResponse.builder()
                                .green(true)
                                .build()
                )
        );
        when(websiteCarbonClient.getCarbon(any())).thenReturn(
                ResponseEntity.ok(
                        WebsiteCarbonResponse.builder()
                                .cleanerThan(0.90)
                                .build()
                )
        );
        when(graderClient.getStats(any())).thenReturn(
                Optional.of(graderResponse())
        );

        var result = calculatorService.calculate(new URL("https://google.com"));
        System.out.println(result);

        assertThat(result).isNotNull();
        assertThat(result).hasSize(8);
        assertThat(result.get(0).getName()).isEqualTo("Alojamiento Ecológico");
        assertThat(result.get(0).getImportance()).isEqualTo(3.97);
        assertThat(result.get(0).getValue()).isEqualTo(95.0);
        assertThat(result.get(0).getRules()).hasSize(2);
        assertThat(result.get(0).getRules().get(0).getName()).isEqualTo("Utilizar servidores alimentados con energía renovable.");
        assertThat(result.get(0).getRules().get(0).getImportance()).isEqualTo(50.0);
        assertThat(result.get(0).getRules().get(0).getValue()).isEqualTo(1.0);
    }

    private GraderResponse graderResponse() {
        GraderResponse response = new GraderResponse();
        response.setDomain("example.com");
        response.setOverallGrade(85); // Setting a constant overall grade for demonstration
        response.setPerformanceAudit(randomPerformanceAudit());
        response.setMobileAudit(randomMobileAudit());
        response.setSeoAudit(randomSeoAudit());
        response.setSecurityAudit(randomSecurityAudit());
        return response;
    }

    // Generate random data for PerformanceAudit
    private GraderResponse.PerformanceAudit randomPerformanceAudit() {
        GraderResponse.PerformanceAudit audit = new GraderResponse.PerformanceAudit();
        audit.setRequestCount(100);
        audit.setLoadTime(120);
        audit.setHasNoRedirects(true);
        audit.setIsCssMinified(true);
        audit.setIsJsMinified(true);
        audit.setAreImagesProperlySized(true);
        audit.setPageSize(2048);
        audit.setWastedSizeProportion(0.15);
        audit.setGrade(90);
        return audit;
    }

    // Generate random data for MobileAudit
    private GraderResponse.MobileAudit randomMobileAudit() {
        GraderResponse.MobileAudit audit = new GraderResponse.MobileAudit();
        audit.setIsSizedForViewport(true);
        audit.setUsesLegibleFontSizes(true);
        audit.setAreTapTargetsProperlySized(true);
        audit.setGrade(80);
        return audit;
    }

    // Generate random data for SeoAudit
    private GraderResponse.SeoAudit randomSeoAudit() {
        GraderResponse.SeoAudit audit = new GraderResponse.SeoAudit();
        audit.setIsIndexable(true);
        audit.setHasMetaDescription(true);
        audit.setHasDescriptiveLinks(true);
        audit.setUsesNoPlugins(true);
        audit.setGrade(75);
        return audit;
    }

    // Generate random data for SecurityAudit
    private GraderResponse.SecurityAudit randomSecurityAudit() {
        GraderResponse.SecurityAudit audit = new GraderResponse.SecurityAudit();
        audit.setUsesHttps(true);
        audit.setHasNoVulnerableLibraries(true);
        audit.setGrade(95);
        return audit;
    }
}
