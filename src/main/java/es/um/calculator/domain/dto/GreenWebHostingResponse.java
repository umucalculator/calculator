package es.um.calculator.domain.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class GreenWebHostingResponse {

    // https://developers.thegreenwebfoundation.org/api/greencheck/v3/check-single-domain/

    private Boolean green;
}
