package es.um.calculator.domain.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class WebsiteCarbonResponse {

    // https://api.websitecarbon.com/

    private Double cleanerThan;
}
