package es.um.calculator.domain.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class GraderResponse {

    // https://website.grader.com/tests/google.com -> from "Network" tab inside "inspect" menu

    private String domain;
    private Integer overallGrade;
    private PerformanceAudit performanceAudit;
    private MobileAudit mobileAudit;
    private SeoAudit seoAudit;
    private SecurityAudit securityAudit;

    @Data
    public static class PerformanceAudit {
        private Integer requestCount;
        private Integer loadTime;
        private Boolean hasNoRedirects;
        private Boolean isCssMinified;
        private Boolean isJsMinified;
        private Boolean areImagesProperlySized;
        private Integer pageSize;
        private Double wastedSizeProportion;
        private Integer grade;
    }

    @Data
    public static class MobileAudit {
        private Boolean isSizedForViewport;
        private Boolean usesLegibleFontSizes;
        private Boolean areTapTargetsProperlySized;
        private Integer grade;
    }

    @Data
    public static class SeoAudit {
        private Boolean isIndexable;
        private Boolean hasMetaDescription;
        private Boolean hasDescriptiveLinks;
        private Boolean usesNoPlugins;
        private Integer grade;
    }

    @Data
    public static class SecurityAudit {
        private Boolean usesHttps;
        private Boolean hasNoVulnerableLibraries;
        private Integer grade;
    }
}
