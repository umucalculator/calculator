package es.um.calculator.domain.model;

import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class RuleGroupModel {

    private String name;
    private Double importance;
    private Double value;
    private List<RuleModel> rules; // 0.0 to 100.0
}
