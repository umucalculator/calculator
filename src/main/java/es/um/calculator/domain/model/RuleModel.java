package es.um.calculator.domain.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class RuleModel {

    private String name;
    private Double importance;
    private Double value; // 0.0 to 1.0
}
