package es.um.calculator.service;

import com.google.api.services.pagespeedonline.v5.model.PagespeedApiPagespeedResponseV5;
import es.um.calculator.domain.model.RuleGroupModel;
import es.um.calculator.domain.model.RuleModel;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class FunctioningService {

    public RuleGroupModel calculate(PagespeedApiPagespeedResponseV5 pagespeedResponse) {
        var data = pagespeedResponse.getLighthouseResult().getAudits();

        var rules = List.of(
                RuleModel.builder()
                        .name("Tiempo del primer renderizado con contenido.")
                        .importance(10.0)
                        .value(((BigDecimal) data.get("first-contentful-paint").getScore()).doubleValue())
                        .build(),
                RuleModel.builder()
                        .name("Tiempo del primer renderizado significativo.")
                        .importance(10.0)
                        .value(((BigDecimal) data.get("first-meaningful-paint").getScore()).doubleValue())
                        .build(),
                RuleModel.builder()
                        .name("Tiempo hasta que la web es interactiva.")
                        .importance(10.0)
                        .value(((BigDecimal) data.get("interactive").getScore()).doubleValue())
                        .build(),
                RuleModel.builder()
                        .name("Latencia máxima para la primera iteración.")
                        .importance(5.0)
                        .value(((BigDecimal) data.get("network-rtt").getScore()).doubleValue())
                        .build(),
                RuleModel.builder()
                        .name("Reducción del tiempo de respuesta del servidor.")
                        .importance(15.0)
                        .value(((BigDecimal) data.get("server-response-time").getScore()).doubleValue())
                        .build(),
                RuleModel.builder()
                        .name("Minificar CSS.")
                        .importance(5.0)
                        .value(((BigDecimal) data.get("unminified-css").getScore()).doubleValue())
                        .build(),
                RuleModel.builder()
                        .name("Minificar JavaScript.")
                        .importance(5.0)
                        .value(((BigDecimal) data.get("unminified-javascript").getScore()).doubleValue())
                        .build(),
                RuleModel.builder()
                        .name("Reducir tiempo de ejecución de JavaScript.")
                        .importance(10.0)
                        .value(((BigDecimal) data.get("duplicated-javascript").getScore()).doubleValue())
                        .build(),
                RuleModel.builder()
                        .name("Priorizar el contenido visible.")
                        .importance(10.0)
                        .value(((BigDecimal) data.get("font-display").getScore()).doubleValue())
                        .build(),
                RuleModel.builder()
                        .name("Eliminar los recursos que bloquean el renderizado.")
                        .importance(10.0)
                        .value(((BigDecimal) data.get("total-blocking-time").getScore()).doubleValue())
                        .build(),
                RuleModel.builder()
                        .name("Comprimir JavaScript y CSS.")
                        .importance(10.0)
                        .value(((BigDecimal) data.get("uses-text-compression").getScore()).doubleValue())
                        .build()
        );

        return RuleGroupModel.builder()
                .name("Funcionamiento")
                .importance(39.29)
                .rules(rules)
                .value(rules.stream()
                        .mapToDouble(rule -> rule.getValue() * rule.getImportance())
                        .sum())
                .build();
    }
}
