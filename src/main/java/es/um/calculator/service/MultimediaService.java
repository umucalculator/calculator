package es.um.calculator.service;

import com.google.api.services.pagespeedonline.v5.model.PagespeedApiPagespeedResponseV5;
import es.um.calculator.domain.model.RuleGroupModel;
import es.um.calculator.domain.model.RuleModel;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class MultimediaService {

    public RuleGroupModel calculate(PagespeedApiPagespeedResponseV5 pagespeedResponse) {
        var data = pagespeedResponse.getLighthouseResult().getAudits();

        var rules = List.of(
                RuleModel.builder()
                        .name("Utilizar imágenes optimizadas con formatos de última generación.")
                        .importance(21.0)
                        .value(((BigDecimal) data.get("modern-image-formats").getScore()).doubleValue())
                        .build(),
                RuleModel.builder()
                        .name("Utilizar un tamaño adecuado para las imágenes.")
                        .importance(16.0)
                        .value(((BigDecimal) data.get("uses-responsive-images").getScore()).doubleValue())
                        .build(),
                RuleModel.builder()
                        .name("Codificación eficaz de las imágenes.")
                        .importance(21.0)
                        .value(((BigDecimal) data.get("uses-optimized-images").getScore()).doubleValue())
                        .build(),
                RuleModel.builder()
                        .name("Posponer la carga de imágenes que no aparecen en pantalla.")
                        .importance(21.0)
                        .value(((BigDecimal) data.get("offscreen-images").getScore()).doubleValue())
                        .build(),
                RuleModel.builder()
                        .name("Usar formatos de vídeo para el contenido animado.")
                        .importance(21.0)
                        .value(((BigDecimal) data.get("efficient-animated-content").getScore()).doubleValue())
                        .build()
        );

        return RuleGroupModel.builder()
                .name("Multimedia")
                .importance(9.92)
                .rules(rules)
                .value(rules.stream()
                        .mapToDouble(rule -> rule.getValue() * rule.getImportance())
                        .sum())
                .build();
    }
}
