package es.um.calculator.service;

import es.um.calculator.domain.dto.GraderResponse;
import es.um.calculator.domain.model.RuleGroupModel;
import es.um.calculator.domain.model.RuleModel;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class SeoService {

    public RuleGroupModel calculate(GraderResponse graderResponse) {
        var seoData = graderResponse.getSeoAudit();

        var rules = List.of(
                RuleModel.builder()
                        .name("La página es indexable por los motores de búsqueda.")
                        .importance(25.0)
                        .value(seoData.getIsIndexable() ? 1.0 : 0.0)
                        .build(),
                RuleModel.builder()
                        .name("Uso de descripciones ’meta’ para explicar el contenido de la página.")
                        .importance(25.0)
                        .value(seoData.getHasMetaDescription() ? 1.0 : 0.0)
                        .build(),
                RuleModel.builder()
                        .name("No depender de plugins del navegador (como Flash).")
                        .importance(10.0)
                        .value(seoData.getUsesNoPlugins() ? 1.0 : 0.0)
                        .build(),
                RuleModel.builder()
                        .name("No mantener enlaces rotos.")
                        .importance(20.0)
                        .value(seoData.getHasDescriptiveLinks() ? 1.0 : 0.0)
                        .build(),
                RuleModel.builder()
                        .name("Usar textos descriptivos para los enlaces.")
                        .importance(20.0)
                        .value(seoData.getHasDescriptiveLinks() ? 1.0 : 0.0)
                        .build()
        );

        return RuleGroupModel.builder()
                .name("Buscabilidad")
                .importance(4.96)
                .rules(rules)
                .value(rules.stream()
                        .mapToDouble(rule -> rule.getValue() * rule.getImportance())
                        .sum())
                .build();
    }
}
