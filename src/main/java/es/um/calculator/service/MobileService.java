package es.um.calculator.service;

import es.um.calculator.domain.dto.GraderResponse;
import es.um.calculator.domain.model.RuleGroupModel;
import es.um.calculator.domain.model.RuleModel;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class MobileService {

    public RuleGroupModel calculate(GraderResponse graderResponse) {
        var mobileData = graderResponse.getMobileAudit();

        var rules = List.of(
                RuleModel.builder()
                        .name("Diseño responsivo para móviles.")
                        .importance(50.0)
                        .value(mobileData.getIsSizedForViewport() ? 1.0 : 0.0)
                        .build(),
                RuleModel.builder()
                        .name("Usar fuentes legibles.")
                        .importance(35.0)
                        .value(mobileData.getUsesLegibleFontSizes() ? 1.0 : 0.0)
                        .build(),
                RuleModel.builder()
                        .name("Los elementos interactivos, como botones, no son demasiado pequeños.")
                        .importance(15.0)
                        .value(mobileData.getAreTapTargetsProperlySized() ? 1.0 : 0.0)
                        .build()
        );

        return RuleGroupModel.builder()
                .name("Diseño Móbil")
                .importance(4.56)
                .rules(rules)
                .value(rules.stream()
                        .mapToDouble(rule -> rule.getValue() * rule.getImportance())
                        .sum())
                .build();
    }
}
