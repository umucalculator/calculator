package es.um.calculator.service;

import com.google.api.client.http.apache.v2.ApacheHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.pagespeedonline.v5.PagespeedInsights;
import com.google.api.services.pagespeedonline.v5.model.PagespeedApiPagespeedResponseV5;
import es.um.calculator.client.GraderClient;
import es.um.calculator.domain.model.RuleGroupModel;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class CalculatorService {

    private final GraderClient graderClient;

    private final GreenHostingService greenHostingService;
    private final ContentService contentService;
    private final SeoService seoService;
    private final MobileService mobileService;
    private final FunctioningService functioningService;
    private final MultimediaService multimediaService;
    private final SecurityService securityService;
    private final HttpService httpService;

    public List<RuleGroupModel> calculate(URL url) {
        log.debug(">>> Starting: {}", url);

        // ---------------

        var graderResponseOpt = graderClient.getStats(url);
        if (graderResponseOpt.isEmpty()) {
            throw new RuntimeException("Failed to get Website Grader stats");
        }

        var graderResponse = graderResponseOpt.get();

        // ---------------

        var requestConfig = RequestConfig.custom()
                .setConnectTimeout(2000000)
                .setSocketTimeout(2000000)
                .build();

        var pagespeedInsights = new PagespeedInsights.Builder(new ApacheHttpTransport(HttpClients.custom()
                .setDefaultRequestConfig(requestConfig)
                .build()), new GsonFactory(), null)
                .build();

        PagespeedApiPagespeedResponseV5 pagespeedResponse;
        try {
            pagespeedResponse = pagespeedInsights.pagespeedapi().runpagespeed(url.toString()).execute();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // ---------------

        var greenHostingGroup = greenHostingService.calculate(url);
        var seoGroup = seoService.calculate(graderResponse);
        var contentGroup = contentService.calculate(graderResponse);
        var mobileGroup = mobileService.calculate(graderResponse);
        var functioningGroup = functioningService.calculate(pagespeedResponse);
        var multimediaGroup = multimediaService.calculate(pagespeedResponse);
        var securityGroup = securityService.calculate(graderResponse);
        var httpGroup = httpService.calculate(pagespeedResponse);

        // ---------------

        log.debug("<<< Finished: {}", url);

        return List.of(
                greenHostingGroup,
                seoGroup,
                contentGroup,
                mobileGroup,
                functioningGroup,
                multimediaGroup,
                securityGroup,
                httpGroup
        );
    }
}
