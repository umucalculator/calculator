package es.um.calculator.service;

import es.um.calculator.domain.dto.GraderResponse;
import es.um.calculator.domain.model.RuleGroupModel;
import es.um.calculator.domain.model.RuleModel;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class SecurityService {

    public RuleGroupModel calculate(GraderResponse graderResponse) {
        var securityData = graderResponse.getSecurityAudit();

        var rules = List.of(
                RuleModel.builder()
                        .name("Utilizar certificados SSL.")
                        .importance(50.0)
                        .value(securityData.getUsesHttps() ? 1.0 : 0.0)
                        .build(),
                RuleModel.builder()
                        .name("Utilizar librerías JavaScript seguras.")
                        .importance(50.0)
                        .value(securityData.getHasNoVulnerableLibraries() ? 1.0 : 0.0)
                        .build()
        );

        return RuleGroupModel.builder()
                .name("Seguridad")
                .importance(3.97)
                .rules(rules)
                .value(rules.stream()
                        .mapToDouble(rule -> rule.getValue() * rule.getImportance())
                        .sum())
                .build();
    }
}
