package es.um.calculator.service;

import es.um.calculator.domain.dto.GraderResponse;
import es.um.calculator.domain.model.RuleGroupModel;
import es.um.calculator.domain.model.RuleModel;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ContentService {

    public RuleGroupModel calculate(GraderResponse graderResponse) {
        var contentData = graderResponse.getPerformanceAudit();
        var seoData = graderResponse.getSeoAudit();

        var rules = List.of(
                RuleModel.builder()
                        .name("Habilitar la compresión de texto.")
                        .importance(25.0)
                        .value(((contentData.getIsJsMinified() ? 0.5 : 0.0) + (contentData.getIsCssMinified() ? 0.5 : 0.0)))
                        .build(),
                RuleModel.builder()
                        .name("Usar un tamaño de página adecuado (inferior a 3MB).")
                        .importance(25.0)
                        .value(contentData.getPageSize() < 3000000 ? 1.0 : contentData.getPageSize() < 6000000 ? 0.5 : 0.0)
                        .build(),
                RuleModel.builder()
                        .name("Usar un tamaño adecuado para las imágenes.")
                        .importance(20.0)
                        .value(contentData.getAreImagesProperlySized() ? 1.0 : 0.0)
                        .build(),
                RuleModel.builder()
                        .name("Utilizar etiquetas de encabezado para distinguir el contenido.")
                        .importance(15.0)
                        .value(seoData.getHasMetaDescription() ? 1.0 : 0.0)
                        .build(),
                RuleModel.builder()
                        .name("Tener un mapa de la web.")
                        .importance(15.0)
                        .value(seoData.getIsIndexable() ? 1.0 : 0.0)
                        .build()
        );

        return RuleGroupModel.builder()
                .name("Contenido")
                .importance(14.85)
                .rules(rules)
                .value(rules.stream()
                        .mapToDouble(rule -> rule.getValue() * rule.getImportance())
                        .sum())
                .build();
    }
}
