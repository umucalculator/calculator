package es.um.calculator.service;

import com.google.api.services.pagespeedonline.v5.model.PagespeedApiPagespeedResponseV5;
import es.um.calculator.domain.model.RuleGroupModel;
import es.um.calculator.domain.model.RuleModel;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class HttpService {

    public RuleGroupModel calculate(PagespeedApiPagespeedResponseV5 pagespeedResponse) {
        var data = pagespeedResponse.getLighthouseResult().getAudits();

        var rules = List.of(
                RuleModel.builder()
                        .name("Aprovechar la caché del navegador.")
                        .importance(25.0)
                        .value(((BigDecimal) data.get("uses-long-cache-ttl").getScore()).doubleValue())
                        .build(),
                RuleModel.builder()
                        .name("Evitar que haya varias redirecciones de página.")
                        .importance(10.0)
                        .value(((BigDecimal) data.get("redirects").getScore()).doubleValue())
                        .build(),
                RuleModel.builder()
                        .name("Número de solicitudes HTTP.")
                        .importance(25.0)
                        .value(((BigDecimal) data.get("network-rtt").getScore()).doubleValue())
                        .build(),
                RuleModel.builder()
                        .name("Utilizar un ancho de banda bajo.")
                        .importance(15.0)
                        .value(((BigDecimal) data.get("network-rtt").getScore()).doubleValue())
                        .build(),
                RuleModel.builder()
                        .name("Indice de velocidad.")
                        .importance(10.0)
                        .value(((BigDecimal) data.get("speed-index").getScore()).doubleValue())
                        .build(),
                RuleModel.builder()
                        .name("Primer tiempo inactivo de la CPU.")
                        .importance(15.0)
                        .value(((BigDecimal) data.get("interactive").getScore()).doubleValue())
                        .build()
        );

        return RuleGroupModel.builder()
                .name("HTTP")
                .importance(9.92)
                .rules(rules)
                .value(rules.stream()
                        .mapToDouble(rule -> rule.getValue() * rule.getImportance())
                        .sum())
                .build();
    }
}
