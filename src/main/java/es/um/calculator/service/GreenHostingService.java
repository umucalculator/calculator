package es.um.calculator.service;

import es.um.calculator.client.GreenWebClient;
import es.um.calculator.client.WebsiteCarbonClient;
import es.um.calculator.domain.model.RuleGroupModel;
import es.um.calculator.domain.model.RuleModel;
import es.um.calculator.exception.FetchException;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
@Slf4j
public class GreenHostingService {

    private final GreenWebClient greenWebClient;
    private final WebsiteCarbonClient websiteCarbonClient;

    public RuleGroupModel calculate(URL url) {
        log.trace("Calculating green hosting for url: {}", url);

        var hostingResponse = greenWebClient.getHosting(url.getHost());
        log.trace("\tHosting response: {}", hostingResponse);

        if (hostingResponse.getStatusCode() != HttpStatus.OK) {
            throw new FetchException("Error getting hosting: " + hostingResponse);
        }

        var isGreenHosted = Objects.requireNonNull(hostingResponse.getBody()).getGreen();

        // -----------------------------------

        var carbonResponse = websiteCarbonClient.getCarbon(URLEncoder.encode(url.getHost()));
        log.trace("\tCarbon response: {}", carbonResponse);

        if (carbonResponse.getStatusCode() != HttpStatus.OK) {
            throw new FetchException("Error getting carbon: " + carbonResponse);
        }

        var rules = List.of(
                RuleModel.builder()
                        .name("Utilizar servidores alimentados con energía renovable.")
                        .importance(50.0)
                        .value(isGreenHosted ? 1.0 : 0.0)
                        .build(),
                RuleModel.builder()
                        .name("Cantidad de CO2 emitida por cada visita.")
                        .importance(50.0)
                        .value(Objects.requireNonNull(carbonResponse.getBody()).getCleanerThan())
                        .build()
        );

        return RuleGroupModel.builder()
                .name("Alojamiento Ecológico")
                .importance(3.97)
                .rules(rules)
                .value(rules.stream()
                        .mapToDouble(rule -> rule.getValue() * rule.getImportance())
                        .sum())
                .build();
    }
}
