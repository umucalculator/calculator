package es.um.calculator.controller;

import es.um.calculator.domain.model.RuleGroupModel;
import es.um.calculator.service.CalculatorService;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
public class CalculatorController {

    private final CalculatorService calculatorService;

    @GetMapping(path = "/")
    public ResponseEntity<List<RuleGroupModel>> calculate(@RequestParam(value = "url") String url) {
        URL url_;
        try {
            url_ = new URL(url);
        } catch (MalformedURLException e) {
            log.error("Malformed URL: {}", url, e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok(calculatorService.calculate(url_));
    }
}
