package es.um.calculator.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class FetchException extends RuntimeException {

    public FetchException(String message) {
        super(message);
    }
}
