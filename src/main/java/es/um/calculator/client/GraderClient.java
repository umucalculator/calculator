package es.um.calculator.client;

import es.um.calculator.domain.dto.GraderResponse;

import java.net.URL;
import java.util.Optional;

public interface GraderClient {

    // NOTE:
    // only works when requested **same url** in the website using a browser like firefox first:
    //    after visiting https://website.grader.com/tests/google.com in the browser, google.com can be requested here
    Optional<GraderResponse> getStats(URL url);
}
