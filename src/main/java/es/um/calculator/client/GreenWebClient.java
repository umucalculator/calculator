package es.um.calculator.client;

import es.um.calculator.domain.dto.GreenWebHostingResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(
        name = "greenweb-client",
        url = "${endpoints.greenweb}"
)
public interface GreenWebClient {

    @GetMapping(path = "/greencheck/{hostname}", produces = "application/json")
    ResponseEntity<GreenWebHostingResponse> getHosting(@PathVariable String hostname);
}
