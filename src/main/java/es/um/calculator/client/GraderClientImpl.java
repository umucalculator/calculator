package es.um.calculator.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import es.um.calculator.domain.dto.GraderResponse;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Optional;

@Component
public class GraderClientImpl implements GraderClient {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public Optional<GraderResponse> getStats(URL url) {
        HttpClient httpClient = HttpClient.newHttpClient();

        HttpRequest request = null;
        try {
            request = HttpRequest.newBuilder()
                    .header("authority", "api.hubspot.com")
                    .header("accept", "application/json")
                    .header("origin", "https://website.grader.com")
                    .header("referer", "https://website.grader.com/")
                    .uri(new URI("https://api.hubspot.com/websitegrader/v2/grade/" + url.getHost())).build();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        try {
            var r = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            var graderResponse = objectMapper.readValue(r.body(), GraderResponse.class);
            return Optional.of(graderResponse);
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
