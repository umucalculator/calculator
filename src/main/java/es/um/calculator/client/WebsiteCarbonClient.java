package es.um.calculator.client;

import es.um.calculator.domain.dto.WebsiteCarbonResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(
        name = "websitecarbon-client",
        url = "${endpoints.websitecarbon}"
)
public interface WebsiteCarbonClient {

    // NOTE: the parameter "url" must be url encoded (https://www.baeldung.com/java-url-encoding-decoding)
    @GetMapping(path = "/site?url={url}", produces = "application/json")
    ResponseEntity<WebsiteCarbonResponse> getCarbon(@PathVariable String url);
}
